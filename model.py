# The model for the skin model classifier

# Import the libraries
import numpy as np
import keras
from keras import backend as K
from keras.layers.core import Dense, Dropout
from keras.optimizers import Adam
from keras.preprocessing.image import ImageDataGenerator
from keras.models import Model
from keras.callbacks import ReduceLROnPlateau, ModelCheckpoint
from sklearn.metrics import confusion_matrix
import itertools
import matplotlib.pyplot as plt

# added these two below for testing purposes only
import pandas as pd
from tqdm import tqdm
from keras.preprocessing import image

# Check if GPU is available
# K.tensorflow_backend._get_available_gpus()

# The paths for the training and validation images
train_path = 'base_dir/train_dir'
valid_path = 'base_dir/val_dir'

# Declare values { took out len(df_train) func - since separating preprocessing.py code into its own step
num_train_samples = 38  # len(df_train)
num_val_samples = 4  # len(df_val)
train_batch_size = 10
val_batch_size = 10
image_size = 224

# Declare how many steps are needed in an iteration
train_steps = np.ceil(num_train_samples / train_batch_size)
val_steps = np.ceil(num_val_samples / val_batch_size)

# Set up generators
train_batches = ImageDataGenerator(
    preprocessing_function= \
        keras.applications.mobilenet.preprocess_input).flow_from_directory(
    train_path,
    target_size=(image_size, image_size),
    batch_size=train_batch_size)

valid_batches = ImageDataGenerator(
    preprocessing_function= \
        keras.applications.mobilenet.preprocess_input).flow_from_directory(
    valid_path,
    target_size=(image_size, image_size),
    batch_size=val_batch_size)

test_batches = ImageDataGenerator(
    preprocessing_function= \
        keras.applications.mobilenet.preprocess_input).flow_from_directory(
    valid_path,
    target_size=(image_size, image_size),
    batch_size=val_batch_size, shuffle=False)

# Create a MobileNet model
mobile = keras.applications.mobilenet.MobileNet()

# See a summary of the layers in the model
mobile.summary()

# How many layers does MobileNet have? { A: 93? }
len(mobile.layers)

# Modify the model
# Exclude the last 5 layers of the model
# Will include all layers up to & including global_average_pooling2d_1
x = mobile.layers[-6].output

# Add a dropout and dense layer for predictions
x = Dropout(0.25)(x)

# Dense(#... corresponds to the num of classes -> thus changed to 1 for 1 class -> ac
# add more when adding more classes
predictions = Dense(1, activation='softmax')(x)

# Create a new model with the new outputs
model = Model(inputs=mobile.input, outputs=predictions)

# See a summary of the new layers in the model
model.summary()

# Freeze the weights of the layers that we aren't training (training the last 23)
for layer in model.layers[:-23]:
    layer.trainable = False

#      #     #
# Train the model
# Define Top2 and Top3 Accuracy
from keras.metrics import categorical_accuracy, top_k_categorical_accuracy

def top_3_accuracy(y_true, y_pred):
    return top_k_categorical_accuracy(y_true, y_pred, k=3)

def top_2_accuracy(y_true, y_pred):
    return top_k_categorical_accuracy(y_true, y_pred, k=2)

# Compile the model - trial& erorr testing w/ diff loss functions
# model.compile(Adam(lr=0.01), loss='categorical_crossentropy', metrics=[categorical_accuracy, top_2_accuracy, top_3_accuracy])
# model.compile(Adam(lr=0.01), loss='mean_squared_error', metrics=['accuracy']) # metrics accuracy found from clothes identifier model
model.compile(Adam(lr=0.01), loss='sparse_categorical_crossentropy', metrics=[categorical_accuracy, top_2_accuracy, top_3_accuracy])


# Get labels that are associated with each index -> output normally = { 'ac: 0, 'nxtClassname': 1, ...}}
print(valid_batches.class_indices)


# Added weights to make the model more sensitive to certain classes
# Used for testing purposes only - will use once more classes are added (also ref ln 137)
"""class_weights={
    0: 1.0,  # ac
    1: 1.0,  # future added class
    ....
}"""

# Declare the filepath for the saved model
filepath = "model.h5"

# Declare a checkpoint to save the best version of the model
checkpoint = ModelCheckpoint(filepath, monitor='val_top_3_accuracy', verbose=1,
                             save_best_only=True, mode='max')

# Reduce the learning rate as the learning stagnates
reduce_lr = ReduceLROnPlateau(monitor='val_top_3_accuracy', factor=0.5, patience=2,
                              verbose=1, mode='max', min_lr=0.00001)

callbacks_list = [checkpoint, reduce_lr]

# Fit the model
history = model.fit_generator(train_batches,
                              steps_per_epoch=train_steps,
                             # class_weight=class_weights,
                              validation_data=valid_batches,
                              validation_steps=val_steps,
                              epochs=30,
                              verbose=1,
                              callbacks=callbacks_list)

# Evaluate the model
# Evaluation of the last epoch

# get metrics name so we can evaluate_generator
model.metrics_names

#^ output should be ['loss', 'categorical_accuracy', 'top_2_accurarcy', 'top_3_accurancy'] <-FOR EX

# print(test_batches)
# print(len(df_val))

# """
# Here last epoch will be used
val_loss, val_cat_acc, val_top_2_acc, val_top_3_acc = \
model.evaluate_generator(test_batches, steps=val_steps)  # tried workers=1 (didn't work)

print('val_loss:', val_loss)
print('val_cat_acc:', val_cat_acc)
print('val_top_2_acc:', val_top_2_acc)
print('val_top_3_acc:', val_top_3_acc)

# Evaluation of the best epoch
model.load_weights('model.h5')

val_loss, val_cat_acc, val_top_2_acc, val_top_3_acc = \
model.evaluate_generator(test_batches, steps=val_steps)   # steps=val_steps

print('val_loss:', val_loss)
print('val_cat_acc:', val_cat_acc)
print('val_top_2_acc:', val_top_2_acc)
print('val_top_3_acc:', val_top_3_acc) #"""


# # #
# Create a confusion matrix of the test images
test_labels = test_batches.classes

# Print label associated w/ each class (for testing purposes)
test_batches.class_indices

# Make predictions ( ex. 938/938 - 15s 16ms/step )
predictions = model.predict_generator(test_batches, steps=val_steps, verbose=1)

# see predictions shape ex. (938, 7)
predictions.shape

# Declare a function for plotting the confusion matrix
def plot_confusion_matrix(cm, classes, normalize=False, title='Confusion matrix', cmap=plt.cm.Blues):
    """
    This function prints and plots the confusion matrix.
    Normalization can be applied by setting `normalize=True`.
    """
    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
        print("Normalized confusion matrix")
    else:
        print('Confusion matrix, without normalization')

    print(cm)

    plt.imshow(cm, interpolation='nearest', cmap=cmap)
    plt.title(title)
    plt.colorbar()
    tick_marks = np.arange(len(classes))
    plt.xticks(tick_marks, classes, rotation=45)
    plt.yticks(tick_marks, classes)

    fmt = '.2f' if normalize else 'd'
    thresh = cm.max() / 2.
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        plt.text(j, i, format(cm[i, j], fmt),
                 horizontalalignment="center",
                 color="white" if cm[i, j] > thresh else "black")

    plt.ylabel('True label')
    plt.xlabel('Predicted label')
    plt.tight_layout()

# added to test labels ex. (938,)
test_labels.shape

cm = confusion_matrix(test_labels, predictions.argmax(axis=1))

# add additional classes into cm_plot_labels
cm_plot_labels = ['ac']

plot_confusion_matrix(cm, cm_plot_labels)

# Added additional lines below (ln 234 - 245) for testing purposes

# Get index of class w/ highest probability score
y_pred = np.argmax(predictions, axis=1)

# Get labels of test images
y_true = test_batches.classes

from sklearn.metrics import classification_report

# Generate a classification report
report = classification_report(y_true, y_pred, target_names=cm_plot_labels)

print(report)



